/**
 * Created by alex on 17.11.16.
 */
public class StringVsStringBuilder {

    private static long stringDemo(){
        String teststring = "a+";
        long starttime = System.currentTimeMillis();
        for (int i = 0; i <= 30000; i++){
            teststring = teststring + "b ";
        }
        long stoptime = System.currentTimeMillis();
        //System.out.println(teststring);
        return (stoptime - starttime);
    }

    private static long stringBuilderDemo(){
        StringBuilder teststring = new StringBuilder("a+");
        long starttime = System.currentTimeMillis();
        for (int i = 0; i <= 30000; i++){
            teststring.append("b ");
        }
        long stoptime = System.currentTimeMillis();
        //System.out.println(teststring);
        return (stoptime - starttime);
    }

    private static long stringBufferDemo(){
        StringBuffer teststring = new StringBuffer("a+");
        long starttime = System.currentTimeMillis();
        for (int i = 0; i <= 30000; i++){
            teststring.append("b ");
        }
        long stoptime = System.currentTimeMillis();
        //System.out.println(teststring);
        return (stoptime - starttime);
    }


    public static void main(String[] args){

        System.out.println("String: - " + stringDemo());
        System.out.println("StringBuilder: - " + stringBuilderDemo());
        System.out.println("StringBuffer: - " + stringBufferDemo());
    }
}
