import java.util.Set;

/**
 * Created by alex on 02.12.16.
 */
public interface Operations {

    public boolean equal(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);
}
