import java.util.*;

/**
 * Created by alex on 02.12.16.
 */
public class SetOperations implements Operations {

    public boolean equal(Set a, Set b) {
        if (a.size() != b.size())
            return false;
        for (Object setElement : a) {
            if (!b.contains(setElement))
                return false;
        }
        return true;
    }
/*
    public <T extends Set<? super AbstractSet>> Set union(T a, T b) {
        T c  = new T (a);
        c.addAll(b);
        return c;
    }
*/
    public <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> c = new HashSet<>(a);
        c.addAll(b);
        return c;
    }

    public Set subtract(Set a, Set b) {
        Set c = new HashSet(a);
        c.removeAll(b);
        return c;
    }

    public Set intersect(Set a, Set b) {
        Set c = new HashSet(a);
        c.retainAll(b);
        return c;
    }

    public Set symmetricSubtract(Set a, Set b) {
        Set c = new HashSet(a);
        for (Object setElement : b) {
            if (c.contains(setElement))
                c.remove(setElement);
            else c.add(setElement);
        }
        return c;
    }
}

class SetOperationsTest {
    public static void main(String[] args) {
        SetOperations SO = new SetOperations();
        Set a = new HashSet<>(Arrays.asList(1, 2));
        Set b = new HashSet<>(Arrays.asList(1, 2));

        //Test is equal
        System.out.println("a: " + a);
        System.out.println("b:" + b);
        System.out.println("equal:" + SO.equal(a, b));

        //Test is not equal
        b.add(4);
        a.add(3);
        System.out.println("a: " + a);
        System.out.println("b:" + b);
        System.out.println("equal:" + SO.equal(a, b));

        //Test union
        a.add(0);
        b.add(12);
        System.out.println("a: " + a);
        System.out.println("b:" + b);
        System.out.println("union: " + SO.union(a, b));

        //Test substract
        System.out.println("a: " + a);
        System.out.println("b:" + b);
        System.out.println("subtract: " + SO.subtract(a, b));

        //Test intersect
        a.add(4);
        a.add(5);
        System.out.println("a: " + a);
        System.out.println("b:" + b);
        System.out.println("intersect: " + SO.intersect(a, b));

        //Test symmetricsubstruct
        Set c = new HashSet<>(Arrays.asList(1, 2,3));
        Set d = new HashSet<>(Arrays.asList(2, 3,4));
        System.out.println("c: " + c);
        System.out.println("d:" + d);
        System.out.println("symmetricsubtract: " + SO.symmetricSubtract(c, d));
    }
}
