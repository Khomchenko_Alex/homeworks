package new_tamagochi.Threads;

import new_tamagochi.Animal.Animal;
import new_tamagochi.Exceptions.OtherException;
import new_tamagochi.Exceptions.OverHealthException;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static new_tamagochi.PetList.petList;

/**
 * Created by alex on 25.12.16.
 */
public class MultiThreadsDemo {
    public void playInThreads() {
        ExecutorService taskExecutor = Executors.newFixedThreadPool(3);
        AutoPlayer autoPlayer = new AutoPlayer();
        for( Animal pet: petList) {
                taskExecutor.execute(() -> autoPlayer.autonomousPlay(pet));
            }
        taskExecutor.shutdown();
    }
}
