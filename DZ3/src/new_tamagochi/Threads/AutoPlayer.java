package new_tamagochi.Threads;

import new_tamagochi.Animal.Animal;
import new_tamagochi.Exceptions.OtherException;
import new_tamagochi.Exceptions.OverHealthException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 25.12.16.
 */
public class AutoPlayer {
    public void autonomousPlay(Animal pet) {
        Set<String> resolvedMethods = new TreeSet(Arrays.asList("run", "jump", "fly", "warble", "eat", "sleep", "swim", "blowBubbles"));
        Class clazz = pet.getClass();
        Method[] methods = clazz.getMethods();
        while (pet.getHealth() > 0) {
            int i = (int) (Math.random() * (methods.length));
            try {
                if (resolvedMethods.contains(methods[i].getName())) {
                    System.out.println(pet.toString() + " - " + methods[i].getName());
                    methods[i].invoke(pet);
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                Throwable cause = e.getCause();
                System.out.println(methods[i].getName() + "  " + cause.getMessage() + " из " + pet);
            }
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {

            }
        }
        System.out.println(pet.getClass().getSimpleName() + " выбыл!");
    }
}
