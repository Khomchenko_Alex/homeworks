package new_tamagochi;

import new_tamagochi.Animal.Animal;
import new_tamagochi.Animal.Bird;
import new_tamagochi.Animal.Cat;
import new_tamagochi.Animal.Fish;

import static new_tamagochi.PetList.petList;

/**
 * Created by alex on 09.12.16.
 */
public class PetCreator {
    public static Animal createPet(String pet) {
        switch (pet) {
            case "Cat":
                Cat cat = new Cat();
                cat.setTypeOfPet();
                cat.setNameOfPet();
                petList.add(cat);
                return cat;
            case "Bird":
                Bird bird = new Bird();
                bird.setNameOfPet();
                bird.setTypeOfPet();
                petList.add(bird);
                return bird;
            case "Fish":
                Fish fish = new Fish();
                fish.setNameOfPet();
                fish.setTypeOfPet();
                petList.add(fish);
                return fish;
        }
        return null;
    }
}
