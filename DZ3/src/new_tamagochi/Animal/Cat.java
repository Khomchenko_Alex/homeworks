package new_tamagochi.Animal;

import new_tamagochi.Exceptions.OtherException;
import new_tamagochi.Exceptions.OverHealthException;

import static new_tamagochi.ConsolInput.inputValue;
import static new_tamagochi.PetList.petList;

/**
 * Created by alex on 08.11.16.
 */
public class Cat extends Animal {

    StringBuilder playPetMenuString = new StringBuilder("1 - Покормить 2 - Положить спать 5 - Состояние 0 - Выход");

    public Cat() {
        this.health = 75;
    }

    public int run() throws OtherException {
        if (health <= MIN_HEALTH + 25) {
            System.out.println("Я очень устал!!! В больничку меня....");
            this.health -= 15;
        } else {
            System.out.println("Туда-сюда...");
            this.health -= 15;
        }
        if (isInjured()) {
            this.health -= 20;
            throw new OtherException("Я повредил лапку!");
        }
        return this.health;
    }

    public int jump() throws OtherException {
        if (health <= MIN_HEALTH + 25) {
            System.out.println("Я очень устал!!! В больничку меня....");
            this.health -= 20;
        } else {
            System.out.println("Со стула на диван - с дивана на шкаф...");
            this.health -= 20;
        }
        if (isInjured()) {
            this.health -= 20;
            throw new OtherException("Я повредил лапку!");
        }
        return this.health;
    }

    public void playWithPet() {
        System.out.println("Сейчас активен - " + this.toString());
        playPetMenuString.insert(33, "3 - Бегать 4 - Прыгать ");

        while (health > 0) {
            System.out.println();
            System.out.println("Выберите действие:");
            System.out.println(playPetMenuString);

            try {
                switch (inputValue()) {
                    case 0:
                        System.out.println(petList);
                        return;
                    case 1:
                        this.eat();
                        break;
                    case 2:
                        this.sleep();
                        break;
                    case 3:
                        run();
                        break;
                    case 4:
                        jump();
                        break;
                    case 5:
                        System.out.println(this.toString());
                        break;
                }
            } catch (OverHealthException e) {
                System.out.println("Ой всё...Хватит!");
            } catch (OtherException e) {
                System.out.println("Ой всё...Хватит!");
            }
        }
        System.out.println("Game over! Питомец заболел и его забрали в больницу(...");
        petList.remove(this);
    }
}
