package new_tamagochi.Animal;

import new_tamagochi.Exceptions.OverHealthException;

import static new_tamagochi.ConsolInput.inputValue;
import static new_tamagochi.PetList.petList;


/**
 * Created by alex on 08.11.16.
 */
public class Fish extends Animal {

    StringBuilder playPetMenuString = new StringBuilder("1 - Покормить 2 - Положить спать 5 - Состояние 0 - Выход");

    public Fish() {
        this.health = 35;
    }

    public int swim() {
        if (health <= MIN_HEALTH + 15) {
            System.out.println("Я очень устала!!! В больничку меня....");
            this.health -= 10;
        } else {
            System.out.println("Туда-сюда...");
            this.health -= 10;
        }

        return this.health;
    }

    public int blowBubbles() {
        if (health <= MIN_HEALTH + 15) {
            System.out.println("Я очень устала!!! В больничку меня....");
            this.health -= 5;
        } else {
            System.out.println("Буль-буль...");
            this.health -= 5;
        }
        return this.health;
    }

    public void playWithPet() {
        System.out.println("Сейчас активен - " + this.toString());
        playPetMenuString.insert(33, "3 - Плавать 4 - Поговорить ");
        while (health > 0) {
            System.out.println();
            System.out.println("Выберите действие:");
            System.out.println(playPetMenuString);
            try {
                switch (inputValue()) {
                    case 0:
                        System.out.println(petList);
                        return;
                    case 1:
                        this.eat();
                        break;
                    case 2:
                        this.sleep();
                        break;
                    case 3:
                        swim();
                        break;
                    case 4:
                        blowBubbles();
                        break;
                    case 5:
                        System.out.println(this.toString());
                        break;
                }
            } catch (OverHealthException e) {
                System.out.println("Ой всё...Хватит!");
            }
        }
        System.out.println("Game over! Питомец заболел и его забрали в больницу(...");
        petList.remove(this);
    }
}
