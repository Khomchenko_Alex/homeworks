package new_tamagochi.Animal;

import new_tamagochi.Exceptions.OverHealthException;

import java.io.Serializable;

import static new_tamagochi.ConsolInput.readString;

/**
 * Created by alex on 08.11.16.
 */
abstract public class Animal implements Serializable {

    static final int MAX_HEALTH = 100;
    static final int MIN_HEALTH = 0;

    int health = 50;
    private String typeOfPet;
    private String nameOfPet;

    public String setTypeOfPet(){
        typeOfPet = this.getClass().getSimpleName();
        return typeOfPet;
    }

    public String getTypeOfPet(){
        return this.typeOfPet;
    }

    public String setNameOfPet() {
        nameOfPet = readString("Введите имя :" );
    return nameOfPet;
    }

    public String getNameOfPet() {
        return this.nameOfPet;
    }

    public int getHealth() {
        return this.health;
    }

    boolean isInjured() {
        int i = (int) (Math.random() * 5 + 1);
        if (i == 5) return true;
        else return false;
    }

    public int eat() throws OverHealthException {
        if (health >= MAX_HEALTH) {
            throw new OverHealthException("Я не могу больше есть!!");
        } else {
            System.out.println("Ням-Ням");
            health += 15;
        }
        return health;
    }

    public int sleep() throws OverHealthException {
        if (health >= MAX_HEALTH) {
            health += 0;
            throw new OverHealthException("Я не могу больше спать!");
        } else {
            System.out.println("Хррррр");
            health += 10;
        }
        return health;
    }

    public void playWithPet(){}

    @Override
    public String toString() {
        String info = "Имя: " + this.getNameOfPet() + " тип: " +this.getTypeOfPet() + " , здоровье: " + getHealth();
        return info;
    }
}
