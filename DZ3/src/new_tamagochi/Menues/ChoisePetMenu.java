package new_tamagochi.Menues;

import new_tamagochi.Animal.Animal;

import static new_tamagochi.ConsolInput.inputValue;
import static new_tamagochi.PetCreator.createPet;

/**
 * Created by alex on 09.11.16.
 */
public class ChoisePetMenu {

    public static Animal choiseThePet() {
        System.out.println();
        System.out.println("Выберите питомца:");
        System.out.println("1 - Fish 2 - Bird 3 - Cat 0-Exit");

        StringBuilder thisAnimal = new StringBuilder(" У вас завелось - ");

        while (true) {
            switch (inputValue()) {
                case 0:
                    return null;
                case 1:
                    System.out.println(thisAnimal.append("Fish"));
                    return createPet("Fish");
                case 2:
                    System.out.println(thisAnimal.append("Bird"));
                    return createPet("Bird");
                case 3:
                    System.out.println(thisAnimal.append("Cat"));
                    return createPet("Cat");
                default:
                    System.out.println("Сделайте правильный выбор!");
            }
        }
    }
}