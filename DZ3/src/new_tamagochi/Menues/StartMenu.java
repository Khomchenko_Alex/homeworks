package new_tamagochi.Menues;

import new_tamagochi.Animal.Animal;
import new_tamagochi.IOTamagochi.TamagochiLoader;
import new_tamagochi.IOTamagochi.TamagochiSaver;
import new_tamagochi.Threads.MultiThreadsDemo;

import java.io.IOException;

import static new_tamagochi.ConsolInput.*;
import static new_tamagochi.Menues.ChoisePetMenu.choiseThePet;
import static new_tamagochi.PetList.petList;

/**
 * Created by alex on 09.12.16.
 */
public class StartMenu {

    public static Animal choiseTheAction() throws NullPointerException {
        Animal currentPet = null;
        while (true) {
            System.out.println();
            System.out.println("1 - Создать нового питомца " +
                    "2 - Загрузить питомцев из файла " +
                    "3 - Записать питомцев в файл " +
                    "4 - Выбрать питомца из списка " +
                    "5 - Запустить игры питамцев в мультипоточном режиме " +
                    "0-Exit");
            switch (inputValue()) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    currentPet = choiseThePet();
                    return currentPet;
                case 2:
                    System.out.println(" Load the petlist");
                    TamagochiLoader tl = new TamagochiLoader();
                    try {
                        tl.findFiles("/home/alex/GeekHub/homeworks/","tmg");
                        petList = tl.load(readString("Введите имя файла:"));
                        System.out.println("Список питомцев успешно загружен!");
                    } catch (IOException e) {
                        System.out.println("Ошибка при загрузке из файла!");
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    System.out.println("Save the petlist");
                    TamagochiSaver ts = new TamagochiSaver();
                    try {
                        ts.save(petList, readString("Введите имя файла:"));
                        System.out.println("Список питомцев успешно сохранён!");
                    } catch (IOException e) {
                        System.out.println("Ошибка записи в файл!");
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    System.out.println("Выберите из списка:");
                    if (petList.size() == 0) {
                        System.out.println("Список пуст");
                        break;
                    } else {
                        int i = 0;
                        for (Animal pet : petList) {
                            System.out.println(i + " - " + pet.toString());
                            i++;
                        }
                    }
                    try {
                        currentPet = petList.get(readValue());
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("Нет такого!");
                    }
                    return currentPet;
                case 5:
                    new MultiThreadsDemo().playInThreads();
                    break;
                default:
                    System.out.println("Сделайте правильный выбор!");
            }
        }

    }
}
