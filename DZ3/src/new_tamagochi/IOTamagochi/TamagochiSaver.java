package new_tamagochi.IOTamagochi;

import new_tamagochi.Animal.Animal;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by alex on 10.12.16.
 */
public class TamagochiSaver {
    public void save (ArrayList<Animal> list, String nameOfFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(nameOfFile + ".tmg");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(list);
        oos.flush();
        oos.close();
    }
}
