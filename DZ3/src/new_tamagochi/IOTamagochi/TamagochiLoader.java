package new_tamagochi.IOTamagochi;

import new_tamagochi.Animal.Animal;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by alex on 10.12.16.
 */
public class TamagochiLoader {

    public ArrayList<Animal> load(String nameOfFile) throws IOException {
        ArrayList<Animal> loadList = null;
        FileInputStream fis = new FileInputStream(nameOfFile);
        ObjectInputStream oin = new ObjectInputStream(fis);
        try {
            loadList = (ArrayList<Animal>) oin.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return loadList;
    }

    public static void findFiles(String dir, String ext) {
        File file = new File(dir);
        if(!file.exists()) System.out.println(dir + " папка не существует");
        File[] listFiles = file.listFiles(new MyFileFilter(ext));
        if(listFiles.length == 0){
            System.out.println(dir + " не содержит файлов с расширением " + ext);
        }else{
            for(File f : listFiles)
                System.out.println("Файл: " + dir + File.separator + f.getName());
        }
    }
}

class MyFileFilter implements FilenameFilter {

    private String ext = "tmg";

    public MyFileFilter(String ext){
        this.ext = ext.toLowerCase();
    }
    @Override
    public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(ext);
    }
}
