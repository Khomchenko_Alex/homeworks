package new_tamagochi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by alex on 19.11.16.
 */
public class ConsolInput {

    public static int readValue() {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }

    public static String readString(String invite) {
        Scanner scan = new Scanner(System.in);
        System.out.println(invite);
        return scan.nextLine();
    }

    public static int inputValue() {
        int value = 9;
        boolean validInput = false;
        while (!validInput) {
            try {
                value = readValue();
            } catch (InputMismatchException e) {
            }
            if (value < 0 || value > 9) {
                System.out.println("Будьте внимательней!");
                validInput = false;
            } else validInput = true;
        }
        return value;
    }
}
